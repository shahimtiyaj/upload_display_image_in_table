<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <form action="display.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>

        <div class="form-group">
            <label for="email">Email address:</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>

        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" name="password">
        </div>


        <div class="form-group">
            <label for="address">Address:</label>
            <input type="text" class="form-control" id="address" name="address">
        </div>

        <div class="form-group">
            <label for="mobile">Mobile:</label>
            <input type="number" class="form-control" id="mobile" name="mobile">
        </div>



        <label>Gender:</label>
        <input type="radio" id="male" value="male" name="gender"><label for="male" class="light">Male</label><br>
        <input type="radio" id="female" value="female" name="gender"><label for="female" class="light">Female</label>


        <div>
            <label for="hobby">Choose your Hobbies</label><br>
            php <input type="checkbox" value="Cricket"  name="hobby[]">
            java <input type="checkbox" value="Football" name="hobby[]">
            python <input type="checkbox" value="Singer" name="hobby[]">
        </div>




        <div class="form-group">
            <label for="bday">Birth day:</label>
            <input type="date" class="form-control" id="mobile" name="bday">
        </div>

        <div>
            <label for="day">Bday:</label>
            <select name="Month">
                <option value="January" name="m">January</option>
                <option value="February" name="m">February</option>
                <option selected value="March" name="m">March</option>
            </select>
            <select name="Day">
                <option value="1" name="d">1</option>
                <option value="2" name="d">2</option>
                <option selected value="3" name="d">3</option>
            </select>
            <select name="Year">
                <option value="2013" name="y">2013</option>
                <option value="2012" name="y">2012</option>
                <option selected value="2011" name="y">2011</option>
            </select>
        </div>

        <div class="form-group">
            <label for="image">Choose your profile picture:</label>
            <input type="file" class="form-control-file" id="image" name="image" width="10" height="10">
        </div>


        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>